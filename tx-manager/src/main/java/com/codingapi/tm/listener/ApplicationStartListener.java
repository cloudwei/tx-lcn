package com.codingapi.tm.listener;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.codingapi.tm.Constants;

/**
 * create by lorne on 2017/8/7
 */
@Component
//public class ApplicationStartListener implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {
public class ApplicationStartListener {

	@Value("${server.port}")
	private Integer port;
	
    public void onApplicationEvent() {
        int serverPort = port;
        String ip = getIp();
        Constants.address = ip+":"+serverPort;
    }
//    @Override
//    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
//    	int serverPort = event.getEmbeddedServletContainer().getPort();
//    	String ip = getIp();
//    	Constants.address = ip+":"+serverPort;
//    }



    private String getIp(){
        String host = null;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return host;
    }
}
